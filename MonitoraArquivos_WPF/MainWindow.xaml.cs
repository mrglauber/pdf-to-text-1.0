using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;
using System.Windows;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Controls;



namespace MonitoraArquivos_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public void On_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            ShowChanges(e.ChangeType, e.Name);
            Thread novaNotificacao = new Thread(new ThreadStart(Notify));
            novaNotificacao.SetApartmentState(ApartmentState.STA);
            novaNotificacao.Start();

        }

        private void Notify()
        {
            Notificacao notificacao = new Notificacao();
            notificacao.Show();
        }

        public void ShowChanges(System.IO.WatcherChangeTypes watcherChangeTypes, string name, string oldName = null)
        {
            Dispatcher.BeginInvoke(new Action(() => AddListLine(string.Format("{0} -> {1} - {2}", watcherChangeTypes.ToString(), name, DateTime.Now))));

        }

        public void AddListLine(string texto)
        {
            this.LBoxContent.Items.Add(texto);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            FileSystemWatcher watcher = new FileSystemWatcher(TbResult.Text);
            watcher.Filter = "*.*";

            watcher.IncludeSubdirectories = true;

            watcher.Created += new FileSystemEventHandler(On_Created);
            watcher.EnableRaisingEvents = true;
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();

            folderDlg.ShowNewFolderButton = true;

            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                TbResult.Text = folderDlg.SelectedPath;
            }


        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string[] imagens =
            {
            @"C:\Users\Glauber Marcelino\Pictures\1.jpg",
            @"C:\Users\Glauber Marcelino\Pictures\2.jpg",
            @"C:\Users\Glauber Marcelino\Pictures\3.jpg"
            };

            foreach (string item in imagens)
            {

                //Imagem letreiro
                imgBanner.Source = new BitmapImage(new Uri(item));
                Thread.Sleep(TimeSpan.FromSeconds(3));
            }
            
            
            
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private void btnTraduzRotulos_Click(object sender, RoutedEventArgs e)
        {
            //TextBlock muda o rotulo
            int i = 0;
            foreach (TextBlock tb in FindVisualChildren<TextBlock>(this))
            {
                if (tb.Text != "Traduzir")
                {
                    tb.Text = "Traduz" + i++;
                }

            }
  O     ]B�
    WS�    �