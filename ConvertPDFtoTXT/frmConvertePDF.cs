﻿using System;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;

namespace ConvertPDFtoTXT
{
    public partial class frmConvertePDF : Form
    {
        //Caixa de dialogo 
        OpenFileDialog arqPDF = new OpenFileDialog();
        string pdfText;
        public frmConvertePDF()
        {
            InitializeComponent();
        }

        private void btnConverter_Click(object sender, EventArgs e)
        {
           rtbResultadoConvercao.Text =  ConvertePDF(txtFile.Text);
        }

        private string ConvertePDF(object txt)
        {
            //Instancia para o ItextSharp
                PdfReader pdfreader = new PdfReader(arqPDF.FileName);
            //Armazenará os dados lidos do arquivo PDF
                pdfText = string.Empty;
            //Laço para leitura de todas as paginas do PDF
                for (int i = 1; i <= pdfreader.NumberOfPages; i++)
                {
                    ITextExtractionStrategy itextextStrat = new SimpleTextExtractionStrategy();
                    
                    string extractText = PdfTextExtractor.GetTextFromPage(pdfreader, i, itextextStrat);
                    
                    //Extrai o texto com a codificação correta (Pegando acentos)
                    extractText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(extractText)));
                    pdfText += extractText;
                    pdfreader.Close();
                }
                return pdfText;
            
        }

        private void btnSalvarTxt_Click(object sender, EventArgs e)
        {
            SaveTxt(rtbResultadoConvercao.Text);
        }
        /// <summary>
        /// Função para salvar o texto lido do PDF ou o conteudo contido no RichTextBox
        /// </summary>
        /// <param name="saveTextFile"></param>
        private void SaveTxt(string saveTextFile)
        {
            if (!string.IsNullOrEmpty(saveTextFile))
            {

                SaveFileDialog saveTxt = new SaveFileDialog();
                try
                {
                    if (saveTxt.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(saveTxt.FileName, saveTextFile);
                    }
                    MessageBox.Show("Arquivo Salvo com sucesso!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message);
                }
            }
        }
        private void btnSalvarPdf_Click(object sender, EventArgs e)
        {
            //Chamando a função para gerar o PDF atraves do texto
            SavePDF(rtbResultadoConvercao.Text);
        }
        /// <summary>
        /// Função para salvar qualquer arquivo de texto contido no RichTextBox
        /// </summary>
        /// <param name="textPdf">Texto a ser gerado em PDF</param>
        private void SavePDF(string textPdf)
        {
            if (!string.IsNullOrEmpty(textPdf))
            {
                SaveFileDialog savepdf = new SaveFileDialog();
                try
                {
                    if (savepdf.ShowDialog() == DialogResult.OK)
                    {
                        Document documento = new Document(PageSize.A4, 30, 60, 0, 20);
                        PdfWriter.GetInstance(documento, new FileStream(savepdf.FileName, FileMode.Create));
                        documento.Open();
                        documento.Add(new Paragraph(textPdf));
                        documento.Close();
                        MessageBox.Show("Arquivo Salvo com sucesso!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro ao Gerar o pdf: " + ex.Message);
                }
            }
        }
        private void rtbResultadoConvercao_TextChanged(object sender, EventArgs e)
        {
            //Verifica se o conteudo do RichtextBox esta preenchido
            if (rtbResultadoConvercao.Text != "")
            {
                btnSalvarPdf.Enabled = true;
                btnSalvarTxt.Enabled = true;
            }
            else
            {
                btnSalvarPdf.Enabled = false;
                btnSalvarTxt.Enabled = false;
            }

        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            //Carrega o arquivo PDF para ser convertido em texto
            if (arqPDF.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = arqPDF.FileName;
            }
        }
    }
}
