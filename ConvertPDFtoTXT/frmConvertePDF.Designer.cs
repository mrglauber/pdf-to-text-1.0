﻿namespace ConvertPDFtoTXT
{
    partial class frmConvertePDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConverter = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.rtbResultadoConvercao = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalvarTxt = new System.Windows.Forms.Button();
            this.btnSalvarPdf = new System.Windows.Forms.Button();
            this.btnFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnConverter
            // 
            this.btnConverter.Location = new System.Drawing.Point(16, 69);
            this.btnConverter.Margin = new System.Windows.Forms.Padding(4);
            this.btnConverter.Name = "btnConverter";
            this.btnConverter.Size = new System.Drawing.Size(100, 51);
            this.btnConverter.TabIndex = 0;
            this.btnConverter.Text = "Converter";
            this.btnConverter.UseVisualStyleBackColor = true;
            this.btnConverter.Click += new System.EventHandler(this.btnConverter_Click);
            // 
            // txtFile
            // 
            this.txtFile.BackColor = System.Drawing.SystemColors.Info;
            this.txtFile.Enabled = false;
            this.txtFile.Location = new System.Drawing.Point(16, 37);
            this.txtFile.Margin = new System.Windows.Forms.Padding(4);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(513, 22);
            this.txtFile.TabIndex = 1;
            // 
            // rtbResultadoConvercao
            // 
            this.rtbResultadoConvercao.BackColor = System.Drawing.Color.Bisque;
            this.rtbResultadoConvercao.Location = new System.Drawing.Point(16, 128);
            this.rtbResultadoConvercao.Margin = new System.Windows.Forms.Padding(4);
            this.rtbResultadoConvercao.Name = "rtbResultadoConvercao";
            this.rtbResultadoConvercao.Size = new System.Drawing.Size(879, 316);
            this.rtbResultadoConvercao.TabIndex = 2;
            this.rtbResultadoConvercao.Text = "";
            this.rtbResultadoConvercao.TextChanged += new System.EventHandler(this.rtbResultadoConvercao_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Caminho PDF:";
            // 
            // btnSalvarTxt
            // 
            this.btnSalvarTxt.Enabled = false;
            this.btnSalvarTxt.Location = new System.Drawing.Point(124, 69);
            this.btnSalvarTxt.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvarTxt.Name = "btnSalvarTxt";
            this.btnSalvarTxt.Size = new System.Drawing.Size(100, 51);
            this.btnSalvarTxt.TabIndex = 4;
            this.btnSalvarTxt.Text = "Salvar Txt";
            this.btnSalvarTxt.UseVisualStyleBackColor = true;
            this.btnSalvarTxt.Click += new System.EventHandler(this.btnSalvarTxt_Click);
            // 
            // btnSalvarPdf
            // 
            this.btnSalvarPdf.Enabled = false;
            this.btnSalvarPdf.Location = new System.Drawing.Point(232, 69);
            this.btnSalvarPdf.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvarPdf.Name = "btnSalvarPdf";
            this.btnSalvarPdf.Size = new System.Drawing.Size(100, 51);
            this.btnSalvarPdf.TabIndex = 5;
            this.btnSalvarPdf.Text = "Salvar Pdf";
            this.btnSalvarPdf.UseVisualStyleBackColor = true;
            this.btnSalvarPdf.Click += new System.EventHandler(this.btnSalvarPdf_Click);
            // 
            // btnFile
            // 
            this.btnFile.Location = new System.Drawing.Point(537, 35);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(36, 24);
            this.btnFile.TabIndex = 6;
            this.btnFile.Text = "...";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // frmConvertePDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(919, 471);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.btnSalvarPdf);
            this.Controls.Add(this.btnSalvarTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbResultadoConvercao);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.btnConverter);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmConvertePDF";
            this.Text = "Conversão de PDF para TXT";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConverter;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.RichTextBox rtbResultadoConvercao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalvarTxt;
        private System.Windows.Forms.Button btnSalvarPdf;
        private System.Windows.Forms.Button btnFile;
    }
}

